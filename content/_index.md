+++
description = ""
title = "GEAN"
draft = false

+++

<div class="row align-center">
    <div class="col col-8 text-center">
    	<p>We are in the process of founding a new institution.<br>This institution is called GEAN.</p>
<p>GEAN is short for ‘German Effective Alturism Network’.<p>GEAN will facilitate and improve interactions between EA local groups in Germany.<br>And it will facilitate interaction between the EA Community and the German EA world.</p>
    </div>
</div>


<div id="action-buttons">
<a class="button primary big" href="https://forum.effectivealtruism.org/posts/9sCvoRupwK9xdiSPw/announcing-plans-for-a-german-effective-altruism-network">Read about our plans</a>
<p>( EA Forum Post outlining what we want to do, and how )</p>
</div>

<div id="kube-features">
	<h2>What you will find on this page in the future</h2>
	<div class="row gutters">
	  <div class="col col-4 item">
	    <figure>
	      <img alt="calendar" height="128" src="/images/calendar.png" width="128">
	    </figure>
	    <h3>Events</h3>
	    <p>calendars and pages for all workshops, meetups and other activities that the german EA community organizes.</p>
	  </div>
	  <div class="col col-4 item">
	    <figure>
	      <img alt="groups" height="128" src="/images/brightness.png" width="128">
	    </figure>
	    <h3>Group pages</h3>
	    <p>Find local groups, and resources on how to grow an existing group or found a new one.</p>
	  </div>
	  <div class="col col-4 item">
	    <figure>
	      <img alt="transparency" height="128" src="/images/zoomin.png" width="128">
	    </figure>
	    <h3>GEAN documents</h3>
	    <p>We want to make the organizations inner workings as transparent and clear as possible.</p>
	  </div>
	</div>

</div>
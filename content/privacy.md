---
title: "Privacy"
date: 2019-08-02T22:36:40+02:00
draft: false
type: page
---

This site does not use any tracking or cookies.

The web server logs contain the IP adresses of all visitors.
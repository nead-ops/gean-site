---
title: "Imprint"
date: 2019-08-02T22:36:32+02:00
type: page
draft: false
---

(required by german law)

## Verantwortlich für den Inhalt nach § 55 Abs. 2 RStV
<p>Florian Zeidler</p>
<address>c/o ESTATIKA GmbH<br>
Robert-Bosch-Str. 21<br>
48153 Münster</address>